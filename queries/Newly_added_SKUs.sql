select p.name as product_name, date(p.creation_date) as added_date,count(mpis.pk) as tags
from probedata.match_product_in_scene mpis
join static_new.product p
on p.pk = mpis.product_fk
where date(creation_date) >= date(date(now()) - interval 10 day) and p.type = 'SKU'
group by p.name, date(p.creation_date);