select a.DVC+b.SKU as products, a.cat, a.creation_date as date from
(select c.name as cat, count(d.pk) as DVC, date(p.creation_date) as creation_date
from static_new.dvc d
join static_new.product p
on p.pk = d.product_fk
join static_new.category c
on c.pk = p.category_fk
where p.delete_date is null and p.type = 'SKU' and is_active = 1
group by c.name, date(p.creation_date))a,
(select c.name as cat, count(p.pk) as SKU, date(p.creation_date) as creation_date
from static_new.product p
join static_new.category c
on c.pk = p.category_fk
where p.delete_date is null and p.type = 'SKU' and is_active = 1
group by c.name, date(p.creation_date))b 
where a.cat = b.cat and a.creation_date = b.creation_date;