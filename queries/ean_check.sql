SELECT 
    p.name AS product_name, p.ean_code,  substr(p.ean_code,-9) as ean_to_check,COUNT(p.pk) as count_of_skus, c.name as cat
FROM
    static_new.product p
    join static_new.category c on c.pk = p.category_fk
WHERE
    p.delete_date is null and p.is_active = '1' and p.type = 'SKU' and ean_code is not null
GROUP BY substr(p.ean_code,-9)
having count(p.pk) > 1;