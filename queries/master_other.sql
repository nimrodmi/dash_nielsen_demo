SELECT 
    SUM(svs.facings) AS all_tags,
    c.name AS cat,
    b.name AS brand,
    p.type AS sku_type,
    pd.plan_name AS cycle,
    pd.start_date AS date,
    r.name AS retailer
FROM
    probedata.scene s
        JOIN
    probedata.session se ON s.session_uid = se.session_uid
        JOIN
    report.scene_visit_summary svs ON svs.visit_date = se.visit_date
        AND svs.session_uid = se.session_uid
        AND svs.scene_fk = s.pk
        AND svs.store_fk = s.store_fk
        JOIN
    static_new.product p ON p.pk = svs.product_fk
        JOIN
    static_new.brand b ON b.pk = p.brand_fk
        JOIN
    static_new.category c ON c.pk = p.category_fk
        LEFT JOIN
    static.plan_details pd ON pd.start_date <= s.scene_date
        AND pd.end_date >= s.scene_date
        JOIN
    static.stores st ON st.pk = s.store_fk
        LEFT JOIN
    static.retailer r ON r.pk = st.retailer_fk
WHERE
    p.type IN ('SKU' , 'Other')
GROUP BY b.name , p.type , pd.plan_name , r.name