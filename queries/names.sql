SELECT 
    a.*
FROM
    (SELECT 
        p.name AS product_name,
            b.name AS brand,
            c.name AS cat,
            COUNT(p.pk) AS skus
    FROM
        static_new.product p
    JOIN static_new.brand b ON b.pk = p.brand_fk
    JOIN static_new.category c ON c.pk = p.category_fk
    WHERE
        p.delete_date IS NULL
            AND p.is_active = 1
    GROUP BY p.name) a
WHERE
    a.skus > 1
;