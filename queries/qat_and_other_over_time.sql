select count(mpip.product_fk) as all_tags, p.type as sku_type, pv.validated_by, c.name as cat, DATE(pr.validation_time) AS date
from probedata.match_product_in_probe mpip
join static_new.product p 
on mpip.product_fk = p.pk
join probedata.probe_view pv
on mpip.probe_fk = pv.pk
join static_new.category c on c.pk = p.category_fk
JOIN probedata.probe pr ON pr.pk = mpip.probe_fk
where p.type in ('SKU', 'Other') AND mpip.probe_fk NOT IN (SELECT DISTINCT
                pr.pk
            FROM
                probedata.probe_review pr)
group by pv.validated_by, p.type, c.name, DATE(pr.validation_time);

