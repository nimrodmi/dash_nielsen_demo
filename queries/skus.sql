SELECT 
    b.name AS brand,
	p.ean_code,
    p.size,
    p.unit_num,
    c.name AS category,
    p.name AS product_name,
    sc.name AS sub_category,
    p.form_factor
FROM
    static_new.product p
        JOIN
    static_new.brand b ON b.pk = p.brand_fk
        JOIN
    static_new.category c ON c.pk = p.category_fk
        JOIN
    static_new.sub_category sc ON sc.pk = p.sub_category_fk
WHERE
    p.type = 'SKU' AND p.delete_date IS NULL
        AND p.is_active = 1;
