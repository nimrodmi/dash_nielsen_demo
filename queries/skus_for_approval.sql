SELECT 
    sp.pk,
    sp.name 'Product',
    sp.item_code,
    sp.ean_code,
    sp.size,
    sp.unit_of_measure,
    sp.subpackages_num,
    sp.smart_caption,
    sp.type,
    sp.form_factor,
    sb.name 'Brand',
    sm.name 'Manufacturer',
    sc.name 'Category',
    sp.created_by,
    date(sp.creation_date) as date,
    sp.is_active
FROM
    static_new.product sp
        JOIN
    static_new.brand sb ON sb.pk = sp.brand_fk
        JOIN
    static_new.manufacturer sm ON sm.pk = sb.manufacturer_fk
        JOIN
    static_new.category sc ON sc.pk = sp.category_fk
WHERE
    sp.type = 'SKU' AND sp.is_active = 0 and sp.delete_date is null;