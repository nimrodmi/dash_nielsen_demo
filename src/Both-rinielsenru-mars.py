import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
from datetime import timedelta
import pandas as pd

from plotly.graph_objs import *
from dash.dependencies import Input, Output, Event

from get_funcs_rinielsenru_mars import *

app = dash.Dash()
DATA_FOLDER = "../data/raw_rinielsenru-mars/"

####################################
# get functions
####################################

category_names = pd.read_csv(DATA_FOLDER + 'dash_report_names.csv').cat.values

product_size = pd.read_csv(DATA_FOLDER + '/dash_report_skus.csv').rename(
    columns={'size': 'package_size'}).category.values

#########################
# skus = pd.read_csv(DATA_FOLDER + '/dash_report_skus.csv')
ean_check = pd.read_csv(DATA_FOLDER + '/dash_report_ean_check.csv')


hunters = pd.read_csv(
    DATA_FOLDER + '/dash_report_hunters.csv')
hunters_dvc = pd.read_csv(
    DATA_FOLDER + '/dash_report_hunters_dvc.csv')
brands_hunted = pd.read_csv(
    DATA_FOLDER + '/dash_report_brands.csv')
hunters_dvc = hunters_dvc.rename(columns={'skus': 'dvcs'})
hunters_grp = hunters.groupby(['hunter', 'date'], as_index=False).agg({'skus': 'sum'})
hunters_dvc_grp = hunters_dvc.groupby(['hunter', 'date'], as_index=False).agg({'dvcs': 'sum'})

hunters_grouped1 = pd.merge(hunters_grp, hunters_dvc_grp, on=['date', 'hunter'], how='left')
hunters_grouped = pd.merge(hunters_grouped1, brands_hunted, on=['date', 'hunter'], how='left')
hunters_grouped = hunters_grouped.fillna(0)
other_by_hunters_daily_dates = [{'label': c, 'value': c} for c in hunters_grouped.date.unique()]

master_other = pd.read_csv(
    DATA_FOLDER + '/dash_report_master_other.csv')
retailer_categories = [{'label': c, 'value': c} for c in master_other.cat.unique()]

hunters_sku_df = pd.read_csv(
    DATA_FOLDER + '/dash_report_hunters.csv')
skus_and_dvcs1 = pd.read_csv(
    DATA_FOLDER + '/dash_report_hunters_dvc.csv')
hunters_sku_df = hunters_sku_df[hunters_sku_df.is_active == 1]
skus_and_dvcs1 = skus_and_dvcs1[skus_and_dvcs1.is_active == 1]
hunters_cat = hunters_sku_df.groupby(['cat', 'date'], as_index=False).agg({'skus': 'sum'})
hunters_cat = hunters_cat.sort_values(['cat', 'date'])
hunters_cat['skus_shift'] = hunters_cat["skus"].shift(1)
hunters_cat['cat_shift'] = hunters_cat["cat"].shift(1)
hunters_cat['sku_dvc'] = 'sku'
hunters_cat["skus_cumsum"] = hunters_cat.groupby("cat")["skus"].cumsum()

hunters_dvc_cat = skus_and_dvcs1.groupby(['cat', 'date'], as_index=False).agg({'skus': 'sum'})
hunters_dvc_cat = hunters_dvc_cat.sort_values(['cat', 'date'])
hunters_dvc_cat['sku_dvc'] = 'dvc'
hunters_dvc_cat["skus_cumsum"] = hunters_dvc_cat.groupby("cat")["skus"].cumsum()

sku_dvc_set = list(set(hunters_dvc_cat.sku_dvc.tolist() + hunters_cat.sku_dvc.tolist()))

skus_and_dvcs_categories = [{'label': c, 'value': c} for c in sku_dvc_set]

master_qat_and_other_over_time = pd.read_csv(
    DATA_FOLDER + '/dash_report_qat_and_other_over_time.csv')

hunters = pd.read_csv(
    DATA_FOLDER + '/dash_report_hunters.csv')
hunters_categories = [{'label': c, 'value': c} for c in hunters.cat.unique()]

####################################
# graphs for rows
####################################

qats_graph = dcc.Graph(id='qats_graph', animate=False)

hunters_graph = dcc.Graph(id='hunters_graph', animate=False)

other_by_retailer = dcc.Graph(id='other_by_retailer', figure=get_other_by_retailer(), animate=False)

skus_and_dvcs = dcc.Graph(id='skus_and_dvcs', figure=get_skus_and_dvcs(), animate=False)

####################################
# rows
####################################


title = html.H1("Hunt Dashboard")

others_over_time = [html.H2("Other % over time"),
                    html.Div([
                        dcc.Graph(id='others_over_time', figure=get_other_over_time(), animate=False)],
                        className='col-md-6'),
                    html.Div([
                        dcc.Graph(id='other_by_cycle', figure=get_other_by_cycle(), animate=False)],
                        className='col-md-6')]

row1 = html.Div(others_over_time, className='row')

Other_by_category = [html.H2("Other % by category"),
                     html.Div([html.Table(generate_table(get_other_by_category()),
                                          id='other_by_category',
                                          className="table table-bordered")],
                              className='col-md-12')]

row2 = html.Div(Other_by_category, className='row')

other_by_qat = [
    html.H2("Other % by QAT - Last 10 Days"),
    html.Div(
        dcc.Dropdown(id='drp-qats',
                     value=[{'label': c, 'value': c} for c in master_qat_and_other_over_time.cat.unique()][0]["value"],
                     options=[{'label': c, 'value': c} for c in master_qat_and_other_over_time.cat.unique()]),
        className='col-md-2'),
    html.Div(html.Table([], id='tbl-qats', className="table table-bordered"), className='col-md-5'),
    html.Div([qats_graph], className="col-md-5")]

row3 = html.Div(other_by_qat, className='row')

other_by_brands = [html.H2("Other % by Brand"),
                   html.Div(dcc.Dropdown(id='other_by_brands_categories',
                                         options=[{"label": c, "value": c} for c in
                                                  get_other_by_brands().cat.unique()]),
                            className='col-md-2'),
                   html.Div([html.Table(id='other_by_brands_tbl', className="table table-bordered")],
                            className='col-md-10')]

row4 = html.Div(other_by_brands, className='row')

other_by_retailer = [html.H2("Other % by Retailer"),
                     html.Div(
                         dcc.Dropdown(id='drp-other_by_retailer',
                                      value=retailer_categories[0]["value"],
                                      options=retailer_categories),
                         className='col-md-2'),
                     html.Div([other_by_retailer], className='col-md-10')]

row5 = html.Div(other_by_retailer, className='row')

skus_and_dvcs = [html.H2("Number of SKUs and DVCs over time activated"),
                 html.Div([dcc.Dropdown(id='drp-skus_and_dvcs',
                                        value=skus_and_dvcs_categories[0]["value"],
                                        options=skus_and_dvcs_categories)],
                          className='col-md-2'),
                 html.Div([skus_and_dvcs], className='col-md-10')]

row6 = html.Div(skus_and_dvcs, className='row')

names = [html.H2("Suspcted as duplicated according to Product Name"),
         html.Div([dcc.Dropdown(id='drp-names',
                                value=category_names[0],
                                options=[{"label": c, "value": c} for c in set(category_names)])],
                  className='col-md-2'),
         html.Div([html.Table([], id='tbl-names', className='table')], className='col-md-10')]

row7 = html.Div(names, className='row')

size_outliers = [html.H2("Size Outliers"),
                 html.Div([dcc.Dropdown(id='drp-package_size',
                                        value=product_size[0],
                                        options=[{"label": c, "value": c} for c in set(product_size)])],
                          className='col-md-2'),
                 html.Div([html.Table([], id='tbl-package_size', className='table')], className='col-md-10')]

row8 = html.Div(size_outliers, className='row')

form_factor = [html.H2("Form_Factor Outliers"),
               html.Div([dcc.Dropdown(id='drp-form_factor', options=valid_sub_categories())], className='col-md-2'),
               html.Div([html.Table([], id='tbl-form_factor', className='table')], className='col-md-10'),
               dcc.Interval(id='interval-component', interval=60 * 1000)]

row9 = html.Div(form_factor, className='row')

skus_for_approval = [html.H2("SKUs For Approval"),
                     html.Div(
                         dcc.Dropdown(id='skus_for_approval_categories',
                                      value='COFFEE',
                                      options=[
                                          {"label": c, "value": c} for c in get_skus_for_approval().Category.unique()]),
                         className='col-md-2'),
                     html.Div([html.Table(id='skus_for_approval_tbl',
                                          className="table table-bordered")],
                              className='col-md-10')]

row10 = html.Div(skus_for_approval, className='row')

radar = [html.H2("Radar convertion rate and actions"),
         html.Div([dcc.Graph(id='radar',
                             figure=get_radar(),
                             animate=False)],
                  className='col-md-12')]

row11 = html.Div(radar, className='row')

others_hunters_data = [html.H2("Hunted SKUs, DVCs and Brands by Hunter - Daily"),
                       html.Div(
                           dcc.Dropdown(
                               id='other_by_hunters_daily_dates',
                               options=[{"label": c, "value": c} for c in get_other_by_hunters_daily().date.unique()]),
                           className='col-md-2'),
                       html.Div([html.Table(id='other_by_hunters_daily_tbl', className="table table-bordered")],
                                className='col-md-10')]

row12 = html.Div(others_hunters_data, className='row')

container_div = html.Div([title, row1, row2, row3, row4, row5, row6, row7, row8, row9, row10, row11, row12],
                         className='container-fluid')

####################################
# callbacks
####################################

app.layout = container_div

app.css.append_css({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"})
app.css.append_css({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"})
app.scripts.append_script({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"})


@app.callback(dash.dependencies.Output('skus_and_dvcs', 'figure'),
              [dash.dependencies.Input('drp-skus_and_dvcs', 'value')])
def get_skus_and_dvcs(sku_dvc):
    if not sku_dvc:
        return []
    hunter_sku_df = pd.read_csv(
        DATA_FOLDER + '/dash_report_hunters.csv')
    skus_and_dvcs_df = pd.read_csv(
        DATA_FOLDER + '/dash_report_hunters_dvc.csv')
    hunter_sku_df = hunter_sku_df[hunter_sku_df.is_active == 1]
    skus_and_dvcs_df = skus_and_dvcs_df[skus_and_dvcs_df.is_active == 1]
    hunters_cat_df = hunter_sku_df.groupby(['cat', 'date'], as_index=False).agg({'skus': 'sum'})
    hunters_cat_df = hunters_cat_df.sort_values(['cat', 'date'])
    hunters_cat_df['skus_shift'] = hunters_cat_df["skus"].shift(1)
    hunters_cat_df['cat_shift'] = hunters_cat_df["cat"].shift(1)
    hunters_cat_df['sku_dvc'] = 'sku'
    hunters_cat_df["skus_cumsum"] = hunters_cat_df.groupby("cat")["skus"].cumsum()

    hunters_dvc_cat_df = skus_and_dvcs_df.groupby(['cat', 'date'], as_index=False).agg({'skus': 'sum'})
    hunters_dvc_cat_df = hunters_dvc_cat_df.sort_values(['cat', 'date'])
    hunters_dvc_cat_df['sku_dvc'] = 'dvc'
    hunters_dvc_cat_df["skus_cumsum"] = hunters_dvc_cat_df.groupby("cat")["skus"].cumsum()

    frames = [hunters_dvc_cat_df, hunters_cat_df]
    skus_and_dvcs_final = pd.concat(frames)
    skus_and_dvcs_final = skus_and_dvcs_final.sort_values('date')

    skus_and_dvcs_final = skus_and_dvcs_final[skus_and_dvcs_final.sku_dvc == sku_dvc]
    data = []
    grp = skus_and_dvcs_final.groupby('cat')
    for name, g in grp:
        data.append(Scatter(x=g.date, y=g.skus_cumsum, name=name))
    layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='skus'), title="Number of SKUs and DVCs over time",
                    legend=dict(orientation="h"))

    return {'data': data, 'layout': layout}


@app.callback(dash.dependencies.Output('other_by_retailer', 'figure'),
              [dash.dependencies.Input('drp-other_by_retailer', 'value')])
def get_other_by_retailer(category):
    if not category:
        return []
    master = pd.read_csv(
        DATA_FOLDER + '/dash_report_master_other.csv')

    master_retailer_other = master[master.sku_type == 'Other']
    master_retailer_other = master_retailer_other.groupby(['cat', 'retailer', 'cycle', 'date'], as_index=False).agg(
        {'all_tags': 'sum'})
    master_retailer_other = master_retailer_other.rename(columns={'all_tags': 'all_tags_other'})
    master_retailer_sku = master[master.sku_type == 'SKU']
    master_retailer_sku = master_retailer_sku.groupby(['cat', 'retailer', 'cycle', 'date'], as_index=False).agg(
        {'all_tags': 'sum'})
    master_retailer_sku = master_retailer_sku.rename(columns={'all_tags': 'all_tags_sku'})
    retailer_merged = pd.merge(master_retailer_sku, master_retailer_other, on=['cat', 'retailer', 'cycle'], how='left')
    retailer_merged = retailer_merged.fillna(0)
    retailer_merged['other_percentages'] = 100 * (
        retailer_merged.all_tags_other / (retailer_merged.all_tags_other + retailer_merged.all_tags_sku))
    retailer_merged = retailer_merged.sort_values('date_x')

    retailer_merged["other_percentages"] = retailer_merged["other_percentages"].map("{0:.1f}".format)
    retailer_merged = retailer_merged[retailer_merged.cat == category]
    data = []
    grp = retailer_merged.groupby('retailer')
    for name, g in grp:
        data.append(Scatter(x=g.date_x, y=g.other_percentages, name=name))
    layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='Other Percentages'), title="Other % By Retailer",
                    legend=dict(orientation="h"))

    return {'data': data, 'layout': layout}


@app.callback(dash.dependencies.Output('tbl-qats', 'children'), [dash.dependencies.Input('drp-qats', 'value')])
def get_qats(category):
    if not category:
        return []
    qat_and_other_over_time = pd.read_csv(
        DATA_FOLDER + '/dash_report_qat_and_other_over_time.csv')
    qat_and_other_over_time['date'] = pd.to_datetime(qat_and_other_over_time['date'])
    qat_and_other_over_time = qat_and_other_over_time[
        qat_and_other_over_time.date > qat_and_other_over_time.date.max() - timedelta(days=10)]

    master_qat_other = qat_and_other_over_time[qat_and_other_over_time.sku_type == 'Other']
    master_qat_other = master_qat_other.groupby(['cat', 'validated_by'], as_index=False).agg({'all_tags': 'sum'})
    master_qat_other = master_qat_other.rename(columns={'all_tags': 'all_tags_other'})
    master_qat_sku = qat_and_other_over_time[qat_and_other_over_time.sku_type == 'SKU']
    master_qat_sku = master_qat_sku.groupby(['cat', 'validated_by'], as_index=False).agg({'all_tags': 'sum'})
    master_qat_sku = master_qat_sku.rename(columns={'all_tags': 'all_tags_sku'})
    qat = pd.merge(master_qat_sku, master_qat_other, on=['cat', 'validated_by'], how='left')
    qat = qat.fillna(0)
    qat['other_percentages'] = 100 * (qat.all_tags_other / (qat.all_tags_other + qat.all_tags_sku))
    qat = qat.sort_values('other_percentages', ascending=False)
    qat = qat[qat.all_tags_other > 500]
    qat["other_percentages"] = qat["other_percentages"].map("{0:.1f}".format)
    return generate_table(qat[qat.cat == category])


@app.callback(dash.dependencies.Output('qats_graph', 'figure'), [dash.dependencies.Input('drp-qats', 'value')])
def get_qats_graph(category):
    if not category:
        return {}
    qat_and_other_over_time = pd.read_csv(
        DATA_FOLDER + '/dash_report_qat_and_other_over_time.csv')
    qat_and_other_over_time['date'] = pd.to_datetime(qat_and_other_over_time['date'])
    qat_and_other_over_time = qat_and_other_over_time[
        qat_and_other_over_time.date > qat_and_other_over_time.date.max() - timedelta(days=10)]
    master_qat_other = qat_and_other_over_time[qat_and_other_over_time.sku_type == 'Other']
    master_qat_other = master_qat_other.groupby(['cat', 'validated_by'], as_index=False).agg({'all_tags': 'sum'})
    master_qat_other = master_qat_other.rename(columns={'all_tags': 'all_tags_other'})
    master_qat_sku = qat_and_other_over_time[qat_and_other_over_time.sku_type == 'SKU']
    master_qat_sku = master_qat_sku.groupby(['cat', 'validated_by'], as_index=False).agg({'all_tags': 'sum'})
    master_qat_sku = master_qat_sku.rename(columns={'all_tags': 'all_tags_sku'})
    qat = pd.merge(master_qat_sku, master_qat_other, on=['cat', 'validated_by'], how='left')
    qat = qat.fillna(0)
    qat['other_percentages'] = 100 * (qat.all_tags_other / (qat.all_tags_other + qat.all_tags_sku))
    qat = qat.sort_values('other_percentages', ascending=False)
    qat = qat[qat.all_tags_other > 500]

    qat = qat.sort_values('other_percentages', ascending=False)
    qat = qat[qat.cat == category]
    data = [Bar(x=qat.validated_by, y=qat.other_percentages),
            Scatter(x=[qat.validated_by.values[0], qat.validated_by.values[-1]],
                    y=[np.mean(qat.other_percentages), np.mean(qat.other_percentages)],
                    mode='lines')]
    layout = Layout(xaxis=XAxis(title='QATs'), yaxis=YAxis(title='Other %'), title="Other % by QAT")

    return {'data': data, 'layout': layout}


@app.callback(
    dash.dependencies.Output('other_by_brands_tbl', 'children'),
    [dash.dependencies.Input('other_by_brands_categories', 'value')])
def other_by_brands_update_table(category):
    if not category:
        return []

    df = get_other_by_brands()
    df["other_percentages"] = df["other_percentages"].map("{0:.1f}".format)
    # df = df.drop('Unnamed: 0', axis=1)
    return generate_table(df[df.cat == category].sort_values('other_percentages', ascending=False), 30)


@app.callback(
    dash.dependencies.Output('other_by_hunters_daily_tbl', 'children'),
    [dash.dependencies.Input('other_by_hunters_daily_dates', 'value')])
def other_by_hunters_update_table(date):
    if not date:
        return []

    df = get_other_by_hunters_daily()
    # df["other_percentages"] = df["other_percentages"].map("{0:.1f}".format)
    # df = df.drop('Unnamed: 0', axis=1)
    return generate_table(df[df.date == date].sort_values('Total', ascending=False), 30)


@app.callback(
    dash.dependencies.Output('skus_for_approval_tbl', 'children'),
    [dash.dependencies.Input('skus_for_approval_categories', 'value')])
def skus_for_approval_update_table(category):
    if not category:
        return []

    df = get_skus_for_approval()
    df["date"] = df["date"].map("{0:.1f}".format)
    return generate_table(df[df.Category == category].sort_values('date', ascending=False), 30)


@app.callback(dash.dependencies.Output('tbl-names', 'children'), [dash.dependencies.Input('drp-names', 'value')])
def fill_table(category):
    product_names = pd.read_csv(DATA_FOLDER + 'dash_report_names.csv')
    df = product_names[product_names.cat == category]
    return generate_table(df)


@app.callback(dash.dependencies.Output('tbl-package_size', 'children'),
              [dash.dependencies.Input('drp-package_size', 'value')])
def fill_table(category):
    skus_df = pd.read_csv(DATA_FOLDER + '/dash_report_skus.csv')
    skus_df = skus_df.rename(columns={'size': 'package_size'})
    cat_df = skus_df[skus_df.category == category]
    list_of_things = cat_df[cat_df.package_size >= cat_df.package_size.quantile(0.99)]
    return generate_table(list_of_things)


@app.callback(dash.dependencies.Output('tbl-form_factor', 'children'),
              [dash.dependencies.Input('drp-form_factor', 'value')])
def fill_table(subcat):
    if not subcat:
        return []
    skus_df = pd.read_csv(DATA_FOLDER + '/dash_report_skus.csv')
    skus_df = skus_df.rename(columns={'size': 'package_size'})

    return generate_table(compute_all_sub_category_outliers(skus_df)[subcat])


@app.callback(dash.dependencies.Output('drp-form_factor', 'options'),
              events=[dash.dependencies.Event('interval-component', 'interval')])
def update_graph_live():
    return valid_sub_categories()


if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=8051)
