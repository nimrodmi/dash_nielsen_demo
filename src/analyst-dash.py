import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go

import pandas as pd
import os

from plotly.graph_objs import *

def generate_table(dataframe, max_rows=10):
    table_data = [html.Thead(html.Tr([html.Th(col, className='text-center') for col in dataframe.columns]))]
    # Body
    table_data += [html.Tbody(html.Tr([
        html.Td(dataframe.iloc[i][col], className='text-center') for col in dataframe.columns
    ])) for i in range(min(len(dataframe), max_rows))]
    return table_data

app = dash.Dash()
DATA_FOLDER = "../data/raw/"

names = pd.read_csv(DATA_FOLDER + 'names.tsv', delimiter='\t')
skus = pd.read_csv(DATA_FOLDER + '/skus.tsv', delimiter='\t')
ean_check = pd.read_csv(DATA_FOLDER + '/ean_check.tsv', delimiter='\t')

skus = skus.rename(columns={'size': 'package_size'})

def compute_all_sub_category_outliers(skus):
    results = {}
    for subcat in skus.sub_category.unique():
        subcat_df = skus[skus.sub_category == subcat]
        form_factors_sum_sub = subcat_df.groupby(['form_factor', 'sub_category'], as_index=False).agg(
            {'category': 'count'})
        sub_category_count_of_form_factors = skus.groupby(['sub_category'], as_index=False).agg(
            {'form_factor': 'count'})
        sub_category_count_of_form_factors = sub_category_count_of_form_factors.rename(
            columns={'form_factor': 'form_factor_count'})
        form_factor_merged = pd.merge(form_factors_sum_sub, sub_category_count_of_form_factors, on=['sub_category'])
        form_factor_merged['outlier'] = form_factor_merged.category / form_factor_merged.form_factor_count
        form_factor_merged = form_factor_merged[form_factor_merged.outlier < 0.02]
        if form_factor_merged.shape[0] == 0:
            continue
        results[subcat] = pd.merge(form_factor_merged, skus, on=['sub_category', 'form_factor'])
    return results

def valid_sub_categories():
    skus = pd.read_csv(DATA_FOLDER + '/skus.tsv', delimiter='\t')
    skus = skus.rename(columns={'size': 'package_size'})
    ans = compute_all_sub_category_outliers(skus)
    return [{"label": c, "value": c} for c in ans.keys()]


names_row = html.Div([html.H2("Names"), html.Div([dcc.Dropdown(id='drp-names', value=names.cat.values[0], options=[{"label": c, "value":c} for c in names.cat.unique()])], className='col-md-2'),
                      html.Div([html.Table([],id='tbl-names', className='table')], className='col-md-10')], className='row')

package_size_row = html.Div([html.H2("Size Outliers"), html.Div([dcc.Dropdown(id='drp-package_size', value=skus.category.values[0], options=[{"label": c, "value": c} for c in skus.category.unique()])], className='col-md-2'),
                      html.Div([html.Table([], id='tbl-package_size', className='table')], className='col-md-10')], className='row')

form_factor_row = html.Div([html.H2("Form_Factor Outliers"), html.Div([dcc.Dropdown(id='drp-form_factor',options=valid_sub_categories())], className='col-md-2'),
                      html.Div([html.Table([], id='tbl-form_factor', className='table')], className='col-md-10'),
                            dcc.Interval(
                                id='interval-component',
                                interval= 60 * 1000  # in milliseconds
                            )
                            ], className='row')


del names
del skus

container_div = html.Div([names_row, package_size_row, form_factor_row], className='container')

app.layout = container_div

app.css.append_css({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"})
app.css.append_css({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"})
app.scripts.append_script({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"})

@app.callback(dash.dependencies.Output('tbl-names', 'children'),[dash.dependencies.Input('drp-names', 'value')])
def fill_table(category):
    names = pd.read_csv(DATA_FOLDER + 'names.tsv', delimiter='\t')
    df = names[names.cat == category]
    return generate_table(df)


@app.callback(dash.dependencies.Output('tbl-package_size', 'children'),[dash.dependencies.Input('drp-package_size', 'value')])
def fill_table(category):
    skus = pd.read_csv(DATA_FOLDER + '/skus.tsv', delimiter='\t')
    skus = skus.rename(columns={'size': 'package_size'})
    cat_df = skus[skus.category == category]
    list_of_things = cat_df[cat_df.package_size >= cat_df.package_size.quantile(0.99)]
    return generate_table(list_of_things)


@app.callback(dash.dependencies.Output('tbl-form_factor', 'children'),[dash.dependencies.Input('drp-form_factor', 'value')])
def fill_table(subcat):
    if not subcat:
        return []
    skus = pd.read_csv(DATA_FOLDER + '/skus.tsv', delimiter='\t')
    skus = skus.rename(columns={'size': 'package_size'})

    return generate_table(compute_all_sub_category_outliers(skus)[subcat])

@app.callback(dash.dependencies.Output('drp-form_factor', 'options'),
              events=[dash.dependencies.Event('interval-component', 'interval')])
def update_graph_live():
    return valid_sub_categories()
if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=8081)
