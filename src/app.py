import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np

import plotly.graph_objs as go

import pandas as pd
import os

from plotly.graph_objs import *

app = dash.Dash()
DATA_FOLDER = "../data/raw/"

def get_other_over_time():
    Other_over_time = pd.read_csv(os.path.join(DATA_FOLDER,'Other_over_time.tsv'),delimiter='\t')
    data =[]
    grp = Other_over_time.groupby('cat')
    for name, g in grp:
        data.append(Scatter(x=g.date, y=g.per, name=name))
    layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='Other %'), title="Other % By Date Of Tagging", legend=dict(orientation="h"))
    return {'data': data, 'layout':layout}

def get_other_by_cycle():
    other_by_cycle = pd.read_csv(os.path.join(DATA_FOLDER,'other_by_cycle.tsv'), delimiter='\t')
    other_by_cycle = other_by_cycle.sort_values('date_for_cycles')

    data =[]
    grp = other_by_cycle.groupby('cat')
    for name, g in grp:
        data.append(Scatter(x=g.cycle, y=g.other_percentages, name=name))
    layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='Other %'), title="Other % By Cycle", legend=dict(orientation="h"))
    return {'data': data, 'layout':layout}

def get_other_by_brands():
    other_by_brands = pd.read_csv(os.path.join(DATA_FOLDER,'Other_by_brands.tsv'),delimiter='\t')
    other_by_brands = other_by_brands[other_by_brands.count_of_other_tags>20]
    categories = other_by_brands.cat.unique()
    return other_by_brands

def get_other_by_category():
    other_by_category = pd.read_csv(os.path.join(DATA_FOLDER,'Other_by_category.tsv'),delimiter='\t')
    other_by_category["other_percentages"] = other_by_category["other_percentages"].map("{0:.1f}".format)
    other_by_category = other_by_category.sort_values('other_percentages',ascending=False)
    #other_by_category = other_by_category.drop('Unnamed: 0', axis=1)
    return other_by_category

@app.callback(dash.dependencies.Output('tbl-qats', 'children'),[dash.dependencies.Input('drp-qats', 'value')])
def get_qats():
    qats = pd.read_csv(os.path.join(DATA_FOLDER,'QATs.tsv'),delimiter='\t')
    qats = qats[qats.all_tags > 500].sort_values('other_percentages',ascending=False)
    qats["other_percentages"] = qats["other_percentages"].map("{0:.1f}".format)
    #qats = qats.drop('Unnamed: 0', axis=1)
    return qats

@app.callback(dash.dependencies.Output('tbl-qats_graph', 'children'),[dash.dependencies.Input('drp-qats_graph', 'value')])
def get_qats_graph():
    qats = pd.read_csv(os.path.join(DATA_FOLDER, 'QATs.tsv'), delimiter='\t')
    qats = qats[qats.all_tags > 500].sort_values('other_percentages', ascending=False)
    data = [Bar(x=qats.validated_by, y=qats.other_percentages),
            Scatter(x=[qats.validated_by.values[0], qats.validated_by.values[-1]],
                    y=[np.mean(qats.other_percentages), np.mean(qats.other_percentages)],
                    mode='lines')]
    layout = Layout(xaxis=XAxis(title='QATs'), yaxis=YAxis(title='Other %'), title="Other % by QAT - Last 10 Days")
    return {'data': data, 'layout':layout}

def get_skus_and_dvcs():
    skus_and_dvcs = pd.read_csv(os.path.join(DATA_FOLDER, 'SKUs_and_DVCs.tsv'),delimiter='\t')
    skus_and_dvcs.head()

    data =[]
    grp = skus_and_dvcs.groupby('cat')
    for name, g in grp:
        data.append(Scatter(x=g.date, y=g.products, name=name))
    layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='products'), title=" Number SKUs and DVCs over time",legend=dict(orientation="h"))
    return {'data': data, 'layout':layout}

def generate_table(dataframe, max_rows=10):
    table_data = [html.Thead(html.Tr([html.Th(col, className='text-center') for col in dataframe.columns]))]
    # Body
    table_data += [html.Tbody(html.Tr([
        html.Td(dataframe.iloc[i][col], className='text-center') for col in dataframe.columns
    ])) for i in range(min(len(dataframe), max_rows))]
    return table_data

others_over_time = dcc.Graph(id='others_over_time', figure=get_other_over_time(), animate=False)

other_by_cycle = dcc.Graph(id='other_by_cycle', figure=get_other_by_cycle(), animate=False)

qats_graph = dcc.Graph(id='qat_graph', figure=get_qats_graph(), animate=False)

other_by_brands = [html.H2("Other % by Brand"),html.Div(dcc.Dropdown(id='other_by_brands_categories', value='COFFEE', options=[{"label": c, "value":c} for c in get_other_by_brands().cat.unique()]), className='col-md-2'),
html.Div([html.Table(id='other_by_brands_tbl', className="table table-bordered")], className='col-md-10')]

skus_and_dvcs = dcc.Graph(id="skus_and_dvcs",figure=get_skus_and_dvcs(), animate=False)

title = html.H1("Hunt Dashboard")

row1 = html.Div([html.H2("Other % over time"), html.Div([others_over_time], className='col-md-6'), html.Div([other_by_cycle], className='col-md-6')], className='row')

row2 = html.Div([html.H2("Other % by category"), html.Div([html.Table(generate_table(get_other_by_category()),id='other_by_category', className="table table-bordered")], className='col-md-12')], className='row')

# to_check_row = html.Div([html.H2("Other % by QAT - Last 10 Days"), html.Div([dcc.Dropdown(id='drp-qats', value=qats.category.values[0], options=[{"label": c, "value": c} for c in qats.category.unique()])], className='col-md-2'),
#                       html.Div([generate_table(get_qats()), id='qats', className="table table-bordered"), className='col-md-4'),
#                       html.Div([dcc.Dropdown(id='drp-qats_graph', value=qats.category.values[0], options=[{"label": c, "value": c} for c in qats.category.unique()])], className='col-md-2'),
#                       html.Div([qats_graph],className="col-md-4")], className='row')

row25 = html.Div([html.H2("Other % by QAT - Last 10 Days"), html.Div(html.Table(generate_table(get_qats()), id='qats', className="table table-bordered"), className='col-md-6'),
                  html.Div([qats_graph],className="col-md-6")], className='row')

# html.H2("Other % by QAT"), html.Table(generate_table(get_qats()), id='qats', className="table table-bordered")], className='col-md-6'),

row3 = html.Div(other_by_brands, className='row')

row4 = html.Div([html.Div([skus_and_dvcs], className='col-md-12')], className='row')
container_div = html.Div([title, row1, row2, row25, row3, row4], className='container')

app.layout = container_div

app.css.append_css({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"})
app.css.append_css({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"})
app.scripts.append_script({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"})

@app.callback(
    dash.dependencies.Output('other_by_brands_tbl', 'children'),
    [dash.dependencies.Input('other_by_brands_categories', 'value')])
def other_by_brands_update_table(category):
    if not category:
        return []

    df = get_other_by_brands()
    df["percentages"] = df["percentages"].map("{0:.1f}".format)
    #df = df.drop('Unnamed: 0', axis=1)
    return generate_table(df[df.cat == category].sort_values('percentages', ascending=False), 30)

if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=8080)
