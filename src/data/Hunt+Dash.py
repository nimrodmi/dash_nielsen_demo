
# coding: utf-8

# In[42]:

from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from plotly.graph_objs import *
import pandas as pd
import numpy as np


init_notebook_mode()


# In[2]:

from IPython.display import HTML
        
HTML('''<script>
code_show=true; 
function code_toggle() {
 if (code_show){
 $('div.input').hide();
 } else {
 $('div.input').show();
 }
 code_show = !code_show
} 
$( document ).ready(code_toggle);
</script>
<form action="javascript:code_toggle()"><input type="submit" value="Yahel's raw code."></form>''')


# In[6]:

Other_over_time = pd.read_csv('C:\\Users\yahels\\Desktop\\Nielsen\\\Querries\\Other_over_time.tsv',delimiter='\t')


data =[]
grp = Other_over_time.groupby('cat')
for name, g in grp:
    data.append(Scatter(x=g.date, y=g.per, name=name))
layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='Other %'), title="Other % By Time Of Tagging")
iplot({'data': data, 'layout':layout})



# In[48]:

other_by_cycle = pd.read_csv('C:\\Users\yahels\\Desktop\\Nielsen\\\Querries\\other_by_cycle.tsv',delimiter='\t')
other_by_cycle = other_by_cycle.sort_values('date')

data =[]
grp = other_by_cycle.groupby('cat')
for name, g in grp:
    data.append(Scatter(x=g.cycle, y=g.other_percentages, name=name))
layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='Other %'), title="Other % By Cycle")
iplot({'data': data, 'layout':layout})
other_by_cycle.head()


# In[28]:

newly_added_skus = pd.read_csv('C:\\Users\yahels\\Desktop\\Nielsen\\\Querries\\Newly_added_SKUs.tsv',delimiter='\t')


data =[]
grp = newly_added_skus.groupby('product_name')
for name, g in grp:
    data.append(Scatter(x=g.added_date, y=g.tags, name=name))
layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='Tags'), title="New SKUs Tags")
iplot({'data': data, 'layout':layout})



# In[25]:

other_by_brands = pd.read_csv('C:\\Users\yahels\\Desktop\\Nielsen\\\Querries\\Other_by_brands.tsv',delimiter='\t')
other_by_brands = other_by_brands[other_by_brands.count_of_other_tags>20]
categories = other_by_brands.cat.unique()

df_dict = dict()

for category in categories:
    df_dict[category] = other_by_brands[other_by_brands.cat==category]



# In[27]:

for category in categories:
    print df_dict[category].sort_values('percentages', ascending=False).head(30)


# In[30]:

other_by_category = pd.read_csv('C:\\Users\yahels\\Desktop\\Nielsen\\\Querries\\Other_by_category.tsv',delimiter='\t')
other_by_category = other_by_category.sort_values('other_percentages',ascending=False)
other_by_category.head(1000)


# In[35]:

qats = pd.read_csv('C:\\Users\yahels\\Desktop\\Nielsen\\\Querries\\QATs.tsv',delimiter='\t')
qats = qats.sort_values('all_tags',ascending=False)
 
qats.head()


# In[49]:

skus_and_dvcs = pd.read_csv('C:\\Users\yahels\\Desktop\\Nielsen\\\Querries\\SKUs_and_DVCs.tsv',delimiter='\t')
skus_and_dvcs.head()

data =[]
grp = skus_and_dvcs.groupby('cat')
for name, g in grp:
    data.append(Scatter(x=g.date, y=g.products, name=name))
layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='products'), title=" Number SKUs and DVCs over time")
iplot({'data': data, 'layout':layout})

