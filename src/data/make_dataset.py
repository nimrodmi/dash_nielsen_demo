# -*- coding: utf-8 -*-
import os
import click
import logging
from sqlalchemy import create_engine
from dotenv import find_dotenv, load_dotenv
import pandas as pd

#@click.command()
#@click.argument('input_filepath', type=click.Path(exists=True))
#@click.argument('output_filepath', type=click.Path())
#def main(input_filepath, output_filepath):
def main():
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')

    '''
    example of engine to use for extracting data from the DB (mySQL)
    engine = create_engine('mysql+pymysql://{user}:{password}@{server}/'.format(user=os.environ.get("SQL_USER"),password=os.environ.get("SQL_PASSWORD"),server=os.environ.get("SQL_SERVER")), echo=False)
    '''

    engine = create_engine('mysql+pymysql://{user}:{password}@{server}/'.format(user=os.environ.get("SQL_USER"),password=os.environ.get("SQL_PASSWORD"),server=os.environ.get("SQL_SERVER")), echo=False)
    mapping = {"Newly_added_SKUs.sql":"Newly_added_SKUs.tsv",
                "Other_by_brands.sql":"Other_by_brands.tsv",
                "Other_by_category.sql":"Other_by_category.tsv",
                "Other_over_time.sql":"Other_over_time.tsv",
                "QATs.sql":"QATs.tsv",
                "SKUs_and_DVCs.sql":"SKUs_and_DVCs.tsv",
                "other_by_cycle.sql":"other_by_cycle.tsv"}
    for fileName in [qf for qf in os.listdir("queries") if qf in mapping]:
        logger.info('Working on query {0}'.format(fileName))
        with open(os.path.join("queries",fileName), "r") as f:
            query = f.read()
            output_csv = os.path.join("data/raw", mapping[fileName])
            pd.read_sql(query, engine).to_csv(output_csv)

if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
