import pandas as pd
import os
from datetime import timedelta

import dash_html_components as html

from plotly.graph_objs import *
import plotly.graph_objs as go


DATA_FOLDER = "../data/raw/"
####################################
# get functions
####################################


def get_other_over_time():
    master_qat_and_other_over_time = pd.read_csv(
        DATA_FOLDER + '/dash_report_qat_and_other_over_time.csv')
    master_other_over_time_other = master_qat_and_other_over_time[master_qat_and_other_over_time.sku_type == 'Other']
    master_other_over_time_other = master_other_over_time_other.groupby(['cat', 'date'], as_index=False).agg(
        {'all_tags': 'sum'})
    master_other_over_time_other = master_other_over_time_other.rename(columns={'all_tags': 'all_tags_other'})
    master_other_over_time_sku = master_qat_and_other_over_time[master_qat_and_other_over_time.sku_type == 'SKU']
    master_other_over_time_sku = master_other_over_time_sku.groupby(['cat', 'date'], as_index=False).agg(
        {'all_tags': 'sum'})
    master_other_over_time_sku = master_other_over_time_sku.rename(columns={'all_tags': 'all_tags_sku'})
    other_over_time = pd.merge(master_other_over_time_sku, master_other_over_time_other, on=['cat', 'date'], how='left')
    other_over_time = other_over_time.fillna(0)
    other_over_time['other_percentages'] = 100 * (
        other_over_time.all_tags_other / (other_over_time.all_tags_other + other_over_time.all_tags_sku))

    data = []
    grp = other_over_time.groupby('cat')
    for name, g in grp:
        data.append(Scatter(x=g.date, y=g.other_percentages, name=name))
    layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='Other %'), title="Other % By Time Of Tagging",
                    legend=dict(orientation="h"))
    return {'data': data, 'layout': layout}


def get_radar():
    radar = pd.read_csv(
        DATA_FOLDER + '/dash_report_radar.csv')
    radar = radar.groupby(['action', 'date'], as_index=False).agg({'products': 'sum'})
    radar_added = radar[radar.action == 'added']
    radar_added = radar_added.rename(columns={'products': 'added'})
    radar_added = radar_added.drop(['action'], axis=1)

    radar_deleted = radar[radar.action == 'deleted']
    radar_deleted = radar_deleted.rename(columns={'products': 'deleted'})
    radar_deleted = radar_deleted.drop(['action'], axis=1)

    radar_merged = pd.merge(radar_added, radar_deleted, on=['date'], how='left')
    radar_merged['conversion_ratio'] = 100 * (radar_merged.added / (radar_merged.deleted + radar_merged.added))
    radar_merged['total_actions'] = radar_merged.added + radar_merged.deleted
    radar_merged = radar_merged.sort_values('date', ascending=False)

    conversion_ratio = go.Scatter(x=radar_merged.date, y=radar_merged.conversion_ratio, name='Conversion Ratio')
    total_actions = go.Scatter(x=radar_merged.date, y=radar_merged.total_actions, yaxis='y2', name='Total Actions')

    data = [conversion_ratio, total_actions]

    layout = go.Layout(
        yaxis=dict(
            title='Conversion Ratio'
        ),
        yaxis2=dict(
            title='Total Actions',
            overlaying='y',
            side='right'
        )
    )
    return {'data': data, 'layout': layout}


def get_other_by_cycle():
    master = pd.read_csv(
        DATA_FOLDER + '/dash_report_master_other.csv')
    master_cycle_other = master[master.sku_type == 'Other']
    master_cycle_other = master_cycle_other.groupby(['cat', 'cycle', 'date'], as_index=False).agg({'all_tags': 'sum'})
    master_cycle_other = master_cycle_other.rename(columns={'all_tags': 'all_tags_other'})
    master_cycle_sku = master[master.sku_type == 'SKU']
    master_cycle_sku = master_cycle_sku.groupby(['cat', 'cycle', 'date'], as_index=False).agg({'all_tags': 'sum'})
    master_cycle_sku = master_cycle_sku.drop(['date'], axis=1)
    master_cycle_sku = master_cycle_sku.rename(columns={'all_tags': 'all_tags_sku'})
    cycle_merged = pd.merge(master_cycle_sku, master_cycle_other, on=['cat', 'cycle'], how='left')
    cycle_merged = cycle_merged.fillna(0)
    cycle_merged['other_percentages'] = 100 * (
        cycle_merged.all_tags_other / (cycle_merged.all_tags_other + cycle_merged.all_tags_sku))

    cycle_merged = cycle_merged.sort_values(['cat', 'date'])
    data = []
    grp = cycle_merged.groupby('cat')
    for name, g in grp:
        data.append(Scatter(x=g.date, y=g.other_percentages, name=name))

    tick_labels = cycle_merged[['cycle', 'date']].drop_duplicates().sort_values('date').cycle.values
    tick_values = cycle_merged[['cycle', 'date']].drop_duplicates().sort_values('date').date.values
    layout = Layout(xaxis=XAxis(title='Time',
                                showticklabels=True,
                                ticktext=tick_labels,
                                ticks="",
                                tickvals=tick_values
                                ),
                    yaxis=YAxis(title='Other %'), title="Other % By Cycle", legend=dict(orientation="h"))
    return {'data': data, 'layout': layout}


def get_other_by_brands():
    master = pd.read_csv(
        DATA_FOLDER + '/dash_report_master_other.csv')
    master_brands_other = master[master.sku_type == 'Other']
    master_brands_sku = master[master.sku_type == 'SKU']
    master_brands_other = master_brands_other.rename(columns={'all_tags': 'all_tags_other'})
    master_brands_sku = master_brands_sku.rename(columns={'all_tags': 'all_tags_sku'})
    master_brands_other = master_brands_other.groupby(['brand', 'cat'], as_index=False).agg({'all_tags_other': 'sum'})
    master_brands_sku = master_brands_sku.groupby(['brand', 'cat'], as_index=False).agg({'all_tags_sku': 'sum'})
    master_brands_merged = pd.merge(master_brands_other, master_brands_sku, on=['brand', 'cat'], how='left')
    master_brands_merged = master_brands_merged.fillna(0)
    master_brands_merged['other_percentages'] = 100 * (
        master_brands_merged.all_tags_other / (master_brands_merged.all_tags_other + master_brands_merged.all_tags_sku))
    master_brands_merged = master_brands_merged[master_brands_merged.all_tags_other > 20]
    return master_brands_merged


def get_other_by_hunters_daily():
    hunters = pd.read_csv(
        DATA_FOLDER + '/dash_report_hunters.csv')
    hunters_dvc = pd.read_csv(
        DATA_FOLDER + '/dash_report_hunters_dvc.csv')
    brands_hunted = pd.read_csv(
        DATA_FOLDER + '/dash_report_brands.csv')
    hunters_dvc = hunters_dvc.rename(columns={'skus': 'dvcs'})
    hunters_grp = hunters.groupby(['hunter', 'date'], as_index=False).agg({'skus': 'sum'})
    hunters_dvc_grp = hunters_dvc.groupby(['hunter', 'date'], as_index=False).agg({'dvcs': 'sum'})

    hunters_grouped1 = pd.merge(hunters_grp, hunters_dvc_grp, on=['date', 'hunter'], how='left')
    hunters_grouped = pd.merge(hunters_grouped1, brands_hunted, on=['date', 'hunter'], how='left')
    hunters_grouped = hunters_grouped.fillna(0)
    hunters_grouped['dvcs*0.5'] = hunters_grouped.dvcs*0.5
    hunters_grouped['brands*1.5'] = hunters_grouped.brands*1.5

    hunters_grouped['Total'] = hunters_grouped.skus + hunters_grouped.dvcs*0.5 + hunters_grouped.brands*1.5

    hunters_grouped = hunters_grouped.sort_values(['date', 'Total'], ascending=[False, False])
    return hunters_grouped


def get_skus_for_approval():
    skus_for_approval = pd.read_csv(
        DATA_FOLDER + '/dash_report_skus_for_approval.csv')
    skus_for_approval['date'] = pd.to_datetime(skus_for_approval['date'])
    skus_for_approval = skus_for_approval[skus_for_approval.date > skus_for_approval.date.max() - timedelta(days=7)]
    skus_for_approval = skus_for_approval.fillna('')
    skus_for_approval = skus_for_approval.sort_values('date', ascending=False)
    return skus_for_approval


def get_other_by_category():
    master = pd.read_csv(
        DATA_FOLDER + '/dash_report_master_other.csv')
    master_category_other = master[master.sku_type == 'Other']
    master_category_other = master_category_other.groupby(['cat'], as_index=False).agg({'all_tags': 'sum'})
    master_category_other = master_category_other.rename(columns={'all_tags': 'all_tags_other'})
    master_category_sku = master[master.sku_type == 'SKU']
    master_category_sku = master_category_sku.groupby(['cat'], as_index=False).agg({'all_tags': 'sum'})
    master_category_sku = master_category_sku.rename(columns={'all_tags': 'all_tags_sku'})
    category_merged = pd.merge(master_category_other, master_category_sku, on=['cat'], how='left')
    category_merged = category_merged.fillna(0)
    category_merged['other_percentages'] = 100 * (
        category_merged.all_tags_other / (category_merged.all_tags_other + category_merged.all_tags_sku))

    category_merged = category_merged.sort_values('other_percentages', ascending=False)
    category_merged["other_percentages"] = category_merged["other_percentages"].map("{0:.1f}".format)
    return category_merged


def get_other_by_retailer():
    master = pd.read_csv(
        DATA_FOLDER + '/dash_report_master_other.csv')
    master_retailer_other = master[master.sku_type == 'Other']
    master_retailer_other = master_retailer_other.groupby(['cat', 'retailer', 'cycle', 'date'], as_index=False).agg(
        {'all_tags': 'sum'})
    master_retailer_other = master_retailer_other.rename(columns={'all_tags': 'all_tags_other'})
    master_retailer_sku = master[master.sku_type == 'SKU']
    master_retailer_sku = master_retailer_sku.groupby(['cat', 'retailer', 'cycle', 'date'], as_index=False).agg(
        {'all_tags': 'sum'})
    master_retailer_sku = master_retailer_sku.rename(columns={'all_tags': 'all_tags_sku'})
    retailer_merged = pd.merge(master_retailer_sku, master_retailer_other, on=['cat', 'retailer', 'cycle'], how='left')
    retailer_merged = retailer_merged.fillna(0)
    retailer_merged['other_percentages'] = 100 * (
        retailer_merged.all_tags_other / (retailer_merged.all_tags_other + retailer_merged.all_tags_sku))
    retailer_merged = retailer_merged.sort_values('date_x')
    retailer_merged["other_percentages"] = retailer_merged["other_percentages"].map("{0:.1f}".format)

    data = []
    grp = retailer_merged.groupby('retailer')
    for name, g in grp:
        data.append(Scatter(x=g.date_x, y=g.other_percentages, name=name))
    layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='Other Percentages'), title="Other % By Retailer",
                    legend=dict(orientation="h"))

    return {'data': data, 'layout': layout}


def get_skus_and_dvcs():
    skus_and_dvcs = pd.read_csv(
        os.path.join(DATA_FOLDER, 'dash_report_skus_and_dvcs.csv'))
    skus_and_dvcs.head()

    data = []
    grp = skus_and_dvcs.groupby('cat')
    for name, g in grp:
        data.append(Scatter(x=g.date, y=g.products, name=name))
    layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='products'), title=" Number SKUs and DVCs over time",
                    legend=dict(orientation="h"))
    return {'data': data, 'layout': layout}


def generate_table(dataframe, max_rows=10):
    table_data = [html.Thead(html.Tr([html.Th(col, className='text-center') for col in dataframe.columns]))]
    # Body
    table_data += [html.Tbody(html.Tr([
        html.Td(dataframe.iloc[i][col], className='text-center') for col in dataframe.columns
    ])) for i in range(min(len(dataframe), max_rows))]
    return table_data


# def generate_table(dataframe, max_rows=10):
#     table_data = [html.Thead(html.Tr([html.Th(col, className='text-center') for col in dataframe.columns]))]
#     # Body
#     table_data += [html.Tbody(html.Tr([
#         html.Td(dataframe.iloc[i][col], className='text-center') for col in dataframe.columns
#     ])) for i in range(min(len(dataframe), max_rows))]
#     return table_data


def compute_all_sub_category_outliers(skus):
    results = {}
    for subcat in skus.sub_category.unique():
        subcat_df = skus[skus.sub_category == subcat]
        form_factors_sum_sub = subcat_df.groupby(['form_factor', 'sub_category'], as_index=False).agg(
            {'category': 'count'})
        sub_category_count_of_form_factors = skus.groupby(['sub_category'], as_index=False).agg(
            {'form_factor': 'count'})
        sub_category_count_of_form_factors = sub_category_count_of_form_factors.rename(
            columns={'form_factor': 'form_factor_count'})
        form_factor_merged = pd.merge(form_factors_sum_sub, sub_category_count_of_form_factors, on=['sub_category'])
        form_factor_merged['outlier'] = form_factor_merged.category / form_factor_merged.form_factor_count
        form_factor_merged = form_factor_merged[form_factor_merged.outlier < 0.02]
        if form_factor_merged.shape[0] == 0:
            continue
        results[subcat] = pd.merge(form_factor_merged, skus, on=['sub_category', 'form_factor'])
    return results


def valid_sub_categories():
    skus = pd.read_csv(DATA_FOLDER + '/dash_report_skus.csv')
    skus = skus.rename(columns={'size': 'package_size'})
    ans = compute_all_sub_category_outliers(skus)
    return [{"label": c, "value": c} for c in ans.keys()]
