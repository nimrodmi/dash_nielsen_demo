import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
from datetime import timedelta


import plotly.graph_objs as go

import pandas as pd
import os

from plotly.graph_objs import *

app = dash.Dash()
DATA_FOLDER = "../data/raw/"

def get_other_over_time():
    master_qat_and_other_over_time = pd.read_csv(
        DATA_FOLDER + '/qat_and_other_over_time.tsv', delimiter='\t')
    master_other_over_time_other = master_qat_and_other_over_time[master_qat_and_other_over_time.sku_type == 'Other']
    master_other_over_time_other = master_other_over_time_other.groupby(['cat', 'date'], as_index=False).agg(
        {'all_tags': 'sum'})
    master_other_over_time_other = master_other_over_time_other.rename(columns={'all_tags': 'all_tags_other'})
    master_other_over_time_sku = master_qat_and_other_over_time[master_qat_and_other_over_time.sku_type == 'SKU']
    master_other_over_time_sku = master_other_over_time_sku.groupby(['cat', 'date'], as_index=False).agg(
        {'all_tags': 'sum'})
    master_other_over_time_sku = master_other_over_time_sku.rename(columns={'all_tags': 'all_tags_sku'})
    other_over_time = pd.merge(master_other_over_time_sku, master_other_over_time_other, on=['cat', 'date'], how='left')
    other_over_time = other_over_time.fillna(0)
    other_over_time['other_percentages'] = 100 * (
    other_over_time.all_tags_other / (other_over_time.all_tags_other + other_over_time.all_tags_sku))

    data = []
    grp = other_over_time.groupby('cat')
    for name, g in grp:
        data.append(Scatter(x=g.date, y=g.other_percentages, name=name))
    layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='Other %'), title="Other % By Time Of Tagging", legend=dict(orientation="h"))
    return {'data': data, 'layout':layout}

def get_other_by_cycle():
    master = pd.read_csv(
        DATA_FOLDER + '/master_other.tsv', delimiter='\t')
    master_cycle_other = master[master.sku_type == 'Other']
    master_cycle_other = master_cycle_other.groupby(['cat', 'cycle', 'date'], as_index=False).agg({'all_tags': 'sum'})
    master_cycle_other = master_cycle_other.rename(columns={'all_tags': 'all_tags_other'})
    master_cycle_sku = master[master.sku_type == 'SKU']
    master_cycle_sku = master_cycle_sku.groupby(['cat', 'cycle', 'date'], as_index=False).agg({'all_tags': 'sum'})
    master_cycle_sku = master_cycle_sku.rename(columns={'all_tags': 'all_tags_sku'})
    cycle_merged = pd.merge(master_cycle_sku, master_cycle_other, on=['cat', 'cycle'], how='left')
    cycle_merged = cycle_merged.fillna(0)
    cycle_merged['other_percentages'] = 100 * (
    cycle_merged.all_tags_other / (cycle_merged.all_tags_other + cycle_merged.all_tags_sku))

    cycle_merged = cycle_merged.sort_values('date_x')

    data = []
    grp = cycle_merged.groupby('cat')
    for name, g in grp:
        data.append(Scatter(x=g.cycle, y=g.other_percentages, name=name))
    layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='Other %'), title="Other % By Cycle", legend=dict(orientation="h"))
    return {'data': data, 'layout':layout}

def get_other_by_brands():
    master = pd.read_csv(
        DATA_FOLDER + '/master_other.tsv', delimiter='\t')
    master_brands_other = master[master.sku_type == 'Other']
    master_brands_sku = master[master.sku_type == 'SKU']
    master_brands_other = master_brands_other.rename(columns={'all_tags': 'all_tags_other'})
    master_brands_sku = master_brands_sku.rename(columns={'all_tags': 'all_tags_sku'})
    master_brands_other = master_brands_other.groupby(['brand', 'cat'], as_index=False).agg({'all_tags_other': 'sum'})
    master_brands_sku = master_brands_sku.groupby(['brand', 'cat'], as_index=False).agg({'all_tags_sku': 'sum'})
    master_brands_merged = pd.merge(master_brands_other, master_brands_sku, on=['brand', 'cat'], how='left')
    master_brands_merged = master_brands_merged.fillna(0)
    master_brands_merged['other_percentages'] = 100 * (
    master_brands_merged.all_tags_other / (master_brands_merged.all_tags_other + master_brands_merged.all_tags_sku))
    master_brands_merged = master_brands_merged[master_brands_merged.all_tags_other > 20]
    categories = master_brands_merged.cat.unique()
    return master_brands_merged

def get_other_by_category():
    master = pd.read_csv(
        DATA_FOLDER + '/master_other.tsv', delimiter='\t')
    master_category_other = master[master.sku_type == 'Other']
    master_category_other = master_category_other.groupby(['cat'], as_index=False).agg({'all_tags': 'sum'})
    master_category_other = master_category_other.rename(columns={'all_tags': 'all_tags_other'})
    master_category_sku = master[master.sku_type == 'SKU']
    master_category_sku = master_category_sku.groupby(['cat'], as_index=False).agg({'all_tags': 'sum'})
    master_category_sku = master_category_sku.rename(columns={'all_tags': 'all_tags_sku'})
    category_merged = pd.merge(master_category_other, master_category_sku, on=['cat'], how='left')
    category_merged = category_merged.fillna(0)
    category_merged['other_percentages'] = 100 * (
    category_merged.all_tags_other / (category_merged.all_tags_other + category_merged.all_tags_sku))

    category_merged["other_percentages"] = category_merged["other_percentages"].map("{0:.1f}".format)
    category_merged = category_merged.sort_values('other_percentages',ascending=False)
    #other_by_category = other_by_category.drop('Unnamed: 0', axis=1)
    return category_merged


def get_skus_and_dvcs():
    skus_and_dvcs = pd.read_csv(
        os.path.join(DATA_FOLDER, 'SKUs_and_DVCs.tsv'),delimiter='\t')
    skus_and_dvcs.head()

    data =[]
    grp = skus_and_dvcs.groupby('cat')
    for name, g in grp:
        data.append(Scatter(x=g.date, y=g.products, name=name))
    layout = Layout(xaxis=XAxis(title='Time'), yaxis=YAxis(title='products'), title=" Number SKUs and DVCs over time",legend=dict(orientation="h"))
    return {'data': data, 'layout':layout}

def generate_table(dataframe, max_rows=10):
    table_data = [html.Thead(html.Tr([html.Th(col, className='text-center') for col in dataframe.columns]))]
    # Body
    table_data += [html.Tbody(html.Tr([
        html.Td(dataframe.iloc[i][col], className='text-center') for col in dataframe.columns
    ])) for i in range(min(len(dataframe), max_rows))]
    return table_data

others_over_time = dcc.Graph(id='others_over_time', figure=get_other_over_time(), animate=False)

other_by_cycle = dcc.Graph(id='other_by_cycle', figure=get_other_by_cycle(), animate=False)

qats_graph = dcc.Graph(id='qats_graph', animate=False)

other_by_retailer = dcc.Graph(id='other_by_retailer', figure=get_other_by_cycle(), animate=False)

other_by_brands = [html.H2("Other % by Brand"),html.Div(dcc.Dropdown(id='other_by_brands_categories', value='COFFEE', options=[{"label": c, "value":c} for c in get_other_by_brands().cat.unique()]), className='col-md-2'),
html.Div([html.Table(id='other_by_brands_tbl', className="table table-bordered")], className='col-md-10')]

skus_and_dvcs = dcc.Graph(id="skus_and_dvcs",figure=get_skus_and_dvcs(), animate=False)

other_by_retailer = dcc.Graph(id='other_by_retailer', animate=False)

title = html.H1("Hunt Dashboard")

row1 = html.Div([html.H2("Other % over time"), html.Div([others_over_time], className='col-md-6'), html.Div([other_by_cycle], className='col-md-6')], className='row')

row2 = html.Div([html.H2("Other % by category"), html.Div([html.Table(generate_table(get_other_by_category()),id='other_by_category', className="table table-bordered")], className='col-md-12')], className='row')


master_qat_and_other_over_time = pd.read_csv(
        DATA_FOLDER + '/qat_and_other_over_time.tsv', delimiter='\t')

qats_categories = [{'label': c, 'value': c} for c in master_qat_and_other_over_time.cat.unique()]

row25 = html.Div([html.H2("Other % by QAT - Last 10 Days"), html.Div([dcc.Dropdown(id='drp-qats',value=qats_categories[0]["value"],options=qats_categories)], className='col-md-2'),
                  html.Div(html.Table([], id='tbl-qats', className="table table-bordered"), className='col-md-5'),
                  html.Div([qats_graph],className="col-md-5")], className='row')

row3 = html.Div(other_by_brands, className='row')

row4 = html.Div([html.Div([skus_and_dvcs], className='col-md-12')], className='row')

row5 = html.Div([html.H2("Other % by Retailer"), html.Div([dcc.Dropdown(id='drp-other_by_retailer',value=qats_categories[0]["value"],options=qats_categories)], className='col-md-2'),
                html.Div([other_by_retailer], className='col-md-10')], className='row')


container_div = html.Div([title, row1, row2, row25, row3, row5,row4], className='container-fluid')

app.layout = container_div

app.css.append_css({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"})
app.css.append_css({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"})
app.scripts.append_script({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"})


@app.callback(dash.dependencies.Output('other_by_retailer', 'figure'),[dash.dependencies.Input('drp-other_by_retailer', 'value')])
def get_other_by_retailer(category):
    if not category:
        return []
    master = pd.read_csv(
        DATA_FOLDER + '/master_other.tsv', delimiter='\t')

    master_retailer_other = master[master.sku_type == 'Other']
    master_retailer_other = master_retailer_other.groupby(['cat', 'retailer', 'cycle'], as_index=False).agg(
        {'all_tags': 'sum'})
    master_retailer_other = master_retailer_other.rename(columns={'all_tags': 'all_tags_other'})
    master_retailer_sku = master[master.sku_type == 'SKU']
    master_retailer_sku = master_retailer_sku.groupby(['cat', 'retailer', 'cycle'], as_index=False).agg(
        {'all_tags': 'sum'})
    master_retailer_sku = master_retailer_sku.rename(columns={'all_tags': 'all_tags_sku'})
    retailer_merged = pd.merge(master_retailer_sku, master_retailer_other, on=['cat', 'retailer', 'cycle'], how='left')
    retailer_merged = retailer_merged.fillna(0)
    retailer_merged['other_percentages'] = 100 * (
    retailer_merged.all_tags_other / (retailer_merged.all_tags_other + retailer_merged.all_tags_sku))
    retailer_merged = retailer_merged[retailer_merged.cat == category]

    all_categories_list = retailer_merged.cat.unique()

    retailer_list = retailer_merged.retailer.unique()

    for cat in all_categories_list:
        plot_data = dict()

        for retailer in retailer_list:
            current_df = retailer_merged[(retailer_merged.cat == cat) &
                                         (retailer_merged.retailer == retailer)]

            plot_data[retailer] = Scatter(x=current_df.cycle, y=current_df.other_percentages, name=retailer,
                                          mode='markers+lines')

    return {'data': plot_data.values(),
               'layout': Layout(xaxis=XAxis(title='Time'),
                                yaxis=XAxis(title='Other %'),
                                title=str(cat) + " - Other Percentages by Retailers")}

@app.callback(dash.dependencies.Output('tbl-qats', 'children'),[dash.dependencies.Input('drp-qats', 'value')])
def get_qats(category):
    if not category:
        return []
    master_qat_and_other_over_time = pd.read_csv(
        DATA_FOLDER + '/qat_and_other_over_time.tsv', delimiter='\t')
    master_qat_and_other_over_time['date'] = pd.to_datetime(master_qat_and_other_over_time['date'])
    master_qat_and_other_over_time = master_qat_and_other_over_time[
        master_qat_and_other_over_time.date > master_qat_and_other_over_time.date.max() - timedelta(days=10)]
    master_qat_other = master_qat_and_other_over_time[master_qat_and_other_over_time.sku_type == 'Other']
    master_qat_other = master_qat_other.groupby(['cat', 'validated_by'], as_index=False).agg({'all_tags': 'sum'})
    master_qat_other = master_qat_other.rename(columns={'all_tags': 'all_tags_other'})
    master_qat_sku = master_qat_and_other_over_time[master_qat_and_other_over_time.sku_type == 'SKU']
    master_qat_sku = master_qat_sku.groupby(['cat', 'validated_by'], as_index=False).agg({'all_tags': 'sum'})
    master_qat_sku = master_qat_sku.rename(columns={'all_tags': 'all_tags_sku'})
    qat = pd.merge(master_qat_sku, master_qat_other, on=['cat', 'validated_by'], how='left')
    qat = qat.fillna(0)
    qat['other_percentages'] = 100 * (qat.all_tags_other / (qat.all_tags_other + qat.all_tags_sku))
    qat = qat.sort_values('other_percentages', ascending=False)
    qat = qat[qat.all_tags_other > 500]
    qat["other_percentages"] = qat["other_percentages"].map("{0:.1f}".format)
    #qats = qats.drop('Unnamed: 0', axis=1)
    return generate_table(qat[qat.cat == category])

@app.callback(dash.dependencies.Output('qats_graph', 'figure'),[dash.dependencies.Input('drp-qats', 'value')])
def get_qats_graph(category):
    if not category:
        return {}
    master_qat_and_other_over_time = pd.read_csv(
        DATA_FOLDER + '/qat_and_other_over_time.tsv', delimiter='\t')
    master_qat_and_other_over_time['date'] = pd.to_datetime(master_qat_and_other_over_time['date'])
    master_qat_and_other_over_time = master_qat_and_other_over_time[
        master_qat_and_other_over_time.date > master_qat_and_other_over_time.date.max() - timedelta(days=10)]
    master_qat_other = master_qat_and_other_over_time[master_qat_and_other_over_time.sku_type == 'Other']
    master_qat_other = master_qat_other.groupby(['cat', 'validated_by'], as_index=False).agg({'all_tags': 'sum'})
    master_qat_other = master_qat_other.rename(columns={'all_tags': 'all_tags_other'})
    master_qat_sku = master_qat_and_other_over_time[master_qat_and_other_over_time.sku_type == 'SKU']
    master_qat_sku = master_qat_sku.groupby(['cat', 'validated_by'], as_index=False).agg({'all_tags': 'sum'})
    master_qat_sku = master_qat_sku.rename(columns={'all_tags': 'all_tags_sku'})
    qat = pd.merge(master_qat_sku, master_qat_other, on=['cat', 'validated_by'], how='left')
    qat = qat.fillna(0)
    qat['other_percentages'] = 100 * (qat.all_tags_other / (qat.all_tags_other + qat.all_tags_sku))
    qat = qat.sort_values('other_percentages', ascending=False)
    qat = qat[qat.all_tags_other > 500]

    qat = qat.sort_values('other_percentages', ascending=False)
    qat = qat[qat.cat == category]
    data = [Bar(x=qat.validated_by, y=qat.other_percentages),
            Scatter(x=[qat.validated_by.values[0], qat.validated_by.values[-1]],
                    y=[np.mean(qat.other_percentages), np.mean(qat.other_percentages)],
                    mode='lines')]
    layout = Layout(xaxis=XAxis(title='QATs'), yaxis=YAxis(title='Other %'), title="Other % by QAT")
    fig = Figure(data=data, layout=layout)
    return {'data': data, 'layout':layout}


@app.callback(
    dash.dependencies.Output('other_by_brands_tbl', 'children'),
    [dash.dependencies.Input('other_by_brands_categories', 'value')])
def other_by_brands_update_table(category):
    if not category:
        return []

    df = get_other_by_brands()
    df["other_percentages"] = df["other_percentages"].map("{0:.1f}".format)
    #df = df.drop('Unnamed: 0', axis=1)
    return generate_table(df[df.cat == category].sort_values('other_percentages', ascending=False), 30)

if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=8080)
