
import pandas as pd
import datetime


DATA_FOLDER = "C:\Users\yahels\Desktop\Nielsen\Querries\Audit"


def get_progress():
    stores = pd.read_csv(
        DATA_FOLDER + '/stores.csv')
    templates = pd.read_csv(
        DATA_FOLDER + '/templates.csv')
    stores = pd.merge(stores, templates, on=['template'], how='left')
    stores = stores.drop(['category'], axis=1)
    stores["Last Visit Date"] = pd.to_datetime(stores["Last Visit Date"])
    stores = stores.sort_values(['Last Visit Date', 'facings', 'In Progress', 'Missing'],
                                ascending=[False, False, False, False])
    stores_all_from_stores = stores.groupby(['Store Number'], as_index=False).agg(
        {'State': 'first', 'Auditor Full Name': 'first', 'Store Name': 'first', 'Sample': 'first',
         'Auditor Mail Address': 'first', 'Retail Manager': 'first', 'Retail Director': 'first',
         'Last Visit Date': 'first', 'Visit Feedback': 'first'})
    this_month = datetime.datetime.now().month
    store_static = stores.copy()
    store_static = store_static[store_static['Last Visit Date'].dt.month == this_month]
    store_static = store_static.sort_values(['Last Visit Date', 'facings', 'In Progress', 'Missing'],
                                            ascending=[False, False, False, False])
    store_static = store_static.rename(
        columns={'index': 'Visit', 'In Progress': 'In_Progress', 'Successful/ Unsuccessful': 'status'})
    store_static.loc[store_static.Missing == 0, 'Missing'] = 'Missing'
    store_static.loc[store_static.status == 2, 'status'] = 'Unsuccessful'
    store_static.loc[store_static.status == 1, 'status'] = 'Successful'
    store_static.loc[store_static.exclude_status_fk.isnull(), 'exclude_status_fk'] = 'Processing'
    store_static.loc[store_static.In_Progress < 6, 'In_Progress'] = 'Processing'
    store_static['In_Progress'] = store_static.apply(
        lambda row: row.status if row.In_Progress == 6 else row.In_Progress, axis=1)
    store_static['Missing'] = store_static.apply(lambda row: row.In_Progress if row.Missing == 1 else row.Missing,
                                                 axis=1)
    store_static = store_static[store_static.exclude_status_fk != 2]
    store_static['Missing'] = store_static.apply(
        lambda row: row.exclude_status_fk if row.exclude_status_fk == 'Processing' else row.Missing, axis=1)

    store_static = store_static.reset_index()
    store_static = store_static.rename(columns={'index': 'Visit'})
    store_static = store_static.sort_values(['Last Visit Date', 'facings', 'In_Progress', 'Missing'],
                                            ascending=[False, False, False, False])
    store_pivot = pd.pivot_table(store_static, index=['Store Name'], columns='Category', values='Missing',
                                 aggfunc="first")
    store_pivot = store_pivot.reset_index()
    progress = pd.merge(store_pivot, stores_all_from_stores, on='Store Name', how='left')
    progress = progress.rename(columns={'Visit Feedback': 'End Of Visit Feedback', 'Auditor Mail Address': 'Username'})
    progress = progress.sort_values('Last Visit Date', ascending=False)

    list_of_columns = ['State', 'Store Number', 'Store Name', 'Sample', 'Username',
                       'Last Visit Date', 'Body Wash', 'Beer', 'Cereal & Granola', 'COFFEE', 'Pet Food',
                       'Auditor Full Name',
                       'Retail Manager', 'Retail Director',
                       'End Of Visit Feedback']

    new_list_of_columns = [i for i in progress.columns if i in list_of_columns]

    progress = progress[new_list_of_columns]
    progress = progress.sort_values('Last Visit Date', ascending=False)
    stores_all = pd.read_csv(
        DATA_FOLDER + '/stores_all.csv')
    stores_all = stores_all.rename(
        columns={'Store_Number': 'Store Number', 'store_name': 'Store Name', 'last_visit_date': 'Last Visit Date'})
    stores_all['Body Wash'] = 'Missing'
    stores_all['Beer'] = 'Missing'
    stores_all['Cereal & Granola'] = 'Missing'
    stores_all['COFFEE'] = 'Missing'
    stores_all['Pet Food'] = 'Missing'
    stores_all['End Of Visit Feedback'] = 'no_visit_feedback'
    stores_all["Last Visit Date"] = pd.to_datetime(stores_all["Last Visit Date"])
    stores_all['Month'] = stores_all['Last Visit Date'].dt.month
    stores_all['Month'] = stores_all['Month'].fillna('')
    stores_all = stores_all[stores_all.Month != this_month]
    stores_all = stores_all.drop(['Last Visit Date'], axis=1)
    stores_all = stores_all.drop(['Month'], axis=1)
    frames = [stores_all, progress]
    progress_concat = pd.concat(frames)
    progress_concat = progress_concat.sort_values('Last Visit Date', ascending=False)
    progress_concat[['Body Wash', 'Beer', 'Cereal & Granola', 'COFFEE', 'Pet Food']] = progress_concat[
        ['Body Wash', 'Beer', 'Cereal & Granola', 'COFFEE', 'Pet Food']].fillna('Missing')

    progress_concat.loc[
        progress_concat["End Of Visit Feedback"] == 'Missing', 'End Of Visit Feedback'] = 'no_visit_feedback'
    progress_concat = progress_concat[['State', 'Store Number', 'Store Name', 'Sample', 'Username', 'Auditor Full Name',
                                       'Retail Manager', 'Retail Director', 'Last Visit Date', 'Body Wash', 'Beer',
                                       'Cereal & Granola', 'COFFEE', 'Pet Food', 'End Of Visit Feedback']]

    progress_concat = progress_concat[progress_concat.Sample != 'B']
    progress_concat = progress_concat.rename(
        columns={'Username': 'User Email Address', 'Auditor Full Name': 'User Full Name'})

    progress_concat = progress_concat.set_index(['State'])
    progress_concat['Last Visit Date'] = progress_concat['Last Visit Date'].dt.date

    user_df = pd.read_csv(
        DATA_FOLDER + '/Basic_UQ.csv')
    user_df["scene_date"] = pd.to_datetime(user_df["scene_date"])
    user_df = user_df[user_df['scene_date'].dt.month == datetime.datetime.now().month]

    scene_grouped = user_df.groupby(['User', 'Scene pk', 'Scene Flaws', 'scene_date'], as_index=False).agg(
        {'Image pk': 'count'})
    scene_grouped = scene_grouped.groupby(['User', 'Scene Flaws'], as_index=False).agg({'Image pk': 'count'})
    scene_grouped = scene_grouped.sort_values(['User', 'Image pk'], ascending=[False, False])
    scene_grouped = scene_grouped.rename(columns={'Scene Flaws': 'Most Common Mistakes', 'Image pk': 'Count'})
    scene_grouped = scene_grouped[scene_grouped["Most Common Mistakes"] != 'Other']
    scene_grouped = scene_grouped.groupby(['User']).head(3)

    probe_grouped = user_df.groupby(['User', 'Image Flaws', 'Image pk', 'scene_date'], as_index=False).agg(
        {'Scene pk': 'count'})
    probe_grouped = probe_grouped.groupby(['User', 'Image Flaws'], as_index=False).agg({'Scene pk': 'count'})
    probe_grouped = probe_grouped.sort_values(['User', 'Scene pk'], ascending=[False, False])
    probe_grouped = probe_grouped.rename(columns={'Image Flaws': 'Most Common Mistakes', 'Scene pk': 'Count'})
    probe_grouped = probe_grouped[probe_grouped["Most Common Mistakes"] != 'Other']

    probe_grouped = probe_grouped.groupby(['User']).head(3)
    df1 = pd.read_csv(
        DATA_FOLDER + '/df1.csv')
    frames = [probe_grouped, scene_grouped]
    user_concat = pd.concat(frames)
    user_concat = user_concat.sort_values(['User'], ascending=[True])
    user_concat_with_improve = pd.merge(user_concat, df1, on='Most Common Mistakes', how='left')
    user_concat_with_improve = user_concat_with_improve.sort_values(['User', 'Count'], ascending=[True, False])
    user_concat_with_improve = user_concat_with_improve.drop(['Count'], axis=1)

    managers_df = user_df.groupby(['User', 'Retail Manager'], as_index=False).agg({'Scene pk': 'count'})
    full_name_df = user_df.groupby(['User', 'Auditor Full Name'], as_index=False).agg({'Scene pk': 'count'})
    user_static = pd.merge(managers_df, full_name_df, on='User')
    user_static = user_static.drop(['Scene pk_x', 'Scene pk_y'], axis=1)

    user_static = user_static.drop_duplicates(['User'])

    user_concat_with_improve = pd.merge(user_concat_with_improve, user_static, on='User', how='left')

    user_concat_with_improve = user_concat_with_improve.rename(
        columns={'User': 'User E-Mail Address', 'Auditor Full Name': 'User Full Name'})

    user_concat_with_improve = user_concat_with_improve[
        ['User E-Mail Address', 'User Full Name', 'Retail Manager', 'Most Common Mistakes', 'How To Improve']]

    user_concat_with_improve = user_concat_with_improve.set_index(['User E-Mail Address'])

    user_concat_with_improve = user_concat_with_improve[
        user_concat_with_improve["Most Common Mistakes"] != "Unclear price tag"]
    user_concat_with_improve = user_concat_with_improve[user_concat_with_improve["Most Common Mistakes"] != "Other"]
    user_concat_with_improve = user_concat_with_improve[
        user_concat_with_improve["Most Common Mistakes"] != "Suspected Fake"]

    writer = pd.ExcelWriter('C:\\Users\yahels\\Desktop\\Nielsen\\Querries\\Audit\\progress_and_user_quality.xlsx',
                            engine='xlsxwriter')
    progress_concat.to_excel(writer, 'progress')
    user_concat_with_improve.to_excel(writer, 'user_quality')

    writer.sheets["progress"].freeze_panes(1, 0)
    writer.sheets["user_quality"].freeze_panes(1, 0)
    writer.save()
    return writer